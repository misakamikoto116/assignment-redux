import React, {Component} from 'react';

import './AddPerson.css';
// Jadi disini kita pake UI state, karena jika untuk input dia tujuannya untuk mengumpulkan datanya saja.. lalu ketika semua data sudah dikumpul jadi satu.. bisa disatukan ke global statenya di reducer dengan cara tombol personAdded dibawah, dia memberikan 2 param dimana mapDispatchToProps di persons.js ada onAddedPerson yang juga menyediakan 2 param, untuk di eksekusi di reducernya
class AddPerson extends Component {
    state = {
        name: '',
        age: ''
    }

    nameChangedHandler = (e) => {
        this.setState({name: e.target.value})
    }

    ageChangedHandler = (e) => {
        this.setState({age: e.target.value})
    }

    render() {
        return (
            <div className="AddPerson">
                <input 
                    type="text" 
                    placeholder="Name"
                    onChange={this.nameChangedHandler}
                    value={this.state.name}/>
                <input 
                    type="number" 
                    placeholder="Age"
                    onChange={this.ageChangedHandler}
                    value={this.state.age}/>
                <button onClick={() => this.props.personAdded(this.state.name, this.state.age)}>Add Person</button>
            </div>

        )
    }
};

export default AddPerson;